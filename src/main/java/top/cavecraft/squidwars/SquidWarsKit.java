package top.cavecraft.squidwars;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public enum SquidWarsKit {
    TANK(
            new ItemStack[] {
                    new ItemStack(Material.STONE_PICKAXE),
                    new ItemStack(Material.STONE_SPADE),
                    new ItemStack(Material.GOLDEN_APPLE, 3),
                    new ItemStack(Material.IRON_SWORD),
                    new ItemStack(Material.WOOL, 5)
            },
            new ItemStack[] {
                    new ItemStack(Material.GOLDEN_APPLE, 1),
                    new ItemStack(Material.IRON_AXE)
            },
            Armor.DIAMOND,
            Armor.IRON
    ),
    ARCHER(
            new ItemStack[] {
                    new ItemStack(Material.STONE_PICKAXE),
                    new ItemStack(Material.STONE_SPADE),
                    new ItemStack(Material.ARROW, 3),
                    new ItemStack(Material.BOW),
                    new ItemStack(Material.WOOL, 5),
                    new ItemStack(Material.STONE_AXE)
            },
            new ItemStack[] {
                    new ItemStack(Material.BOW),
                    new ItemStack(Material.WOOD_SWORD),
                    new ItemStack(Material.ARROW, 1),
            },
            Armor.LEATHER,
            null
    ),
    RUSHER(
            new ItemStack[] {
                    new ItemStack(Material.STONE_PICKAXE),
                    new ItemStack(Material.STONE_SPADE),
                    new ItemStack(Material.IRON_SWORD),
                    new ItemStack(Material.WOOL, 20)
            },
            new ItemStack[] {
                    new ItemStack(Material.STONE_SWORD),
                    new ItemStack(Material.WOOL, 8)
            },
            PotionEffectType.SPEED,
            Armor.IRON,
            Armor.CHAIN
    );

    PotionEffectType potion = null;
    ItemStack[] items;
    ItemStack[] respawnItems;
    Armor armor = null;
    Armor respawnArmor = null;

    SquidWarsKit(ItemStack[] items, ItemStack[] respawnItems, PotionEffectType potion) {
        this.items = items;
        this.potion = potion;
        this.respawnItems = respawnItems;
    }

    SquidWarsKit(ItemStack[] items, ItemStack[] respawnItems, PotionEffectType potion, Armor armor, Armor respawnArmor) {
        this.items = items;
        this.potion = potion;
        this.respawnItems = respawnItems;
        this.armor = armor;
        this.respawnArmor = respawnArmor;
    }

    SquidWarsKit(ItemStack[] items, ItemStack[] respawnItems, Armor armor, Armor respawnArmor) {
        this.items = items;
        this.respawnItems = respawnItems;
        this.armor = armor;
        this.respawnArmor = respawnArmor;

        if (items[3].getType() == Material.BOW) {
            items[3].addEnchantment(Enchantment.ARROW_DAMAGE, 1);
        }
    }

    SquidWarsKit(ItemStack[] items, ItemStack[] respawnItems) {
        this.items = items;
        this.respawnItems = respawnItems;
    }

    public void setKit(Player player) {
        if (potion != null) {
            player.addPotionEffect(new PotionEffect(potion, 999999, 1));
        }

        player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 999999, 0));

        if (armor != null) {
            armor.applyArmor(player);
        }

        for (ItemStack item : items) {
            player.getInventory().addItem(item);
        }
    }

    public void setRespawnKit(Player player) {
        if (potion != null) {
            player.addPotionEffect(new PotionEffect(potion, 999999, 1));
        }

        player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 999999, 0));

        if (respawnArmor != null) {
            respawnArmor.applyArmor(player);
        }

        for (ItemStack item : respawnItems) {
            player.getInventory().addItem(item);
        }
    }
}
