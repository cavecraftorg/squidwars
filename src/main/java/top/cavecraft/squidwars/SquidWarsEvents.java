package top.cavecraft.squidwars;

import org.bukkit.*;
import org.bukkit.block.BlockState;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Squid;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Wool;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import top.cavecraft.cclib.ICCLib;
import top.cavecraft.cclib.ICCLib.IActionBarMessage;
import top.cavecraft.cclib.ICCLib.IGameUtils.GemAmount;

import static org.bukkit.Bukkit.getScheduler;
import static org.bukkit.Bukkit.getServer;
import static top.cavecraft.squidwars.SquidWars.*;

public class SquidWarsEvents implements Listener {
    @EventHandler
    public void dropItem(PlayerDropItemEvent event) {
        if (!started) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void entityDamage(EntityDamageEvent event) {
        if (!started) {
            event.setCancelled(true);
        } else {
            Location location = event.getEntity().getLocation();
            location.getWorld().playSound(location, Sound.DIG_STONE, 1, 1);
            location.getWorld().spigot().playEffect(location, Effect.TILE_BREAK, Material.REDSTONE_BLOCK.getId(), Material.REDSTONE_BLOCK.getId(), 0.2f, 0.2f, 0.2f, 0.02f, 10, 1);

            if (event.getEntity().getType() == EntityType.SQUID) {
                Squid squid = (Squid) event.getEntity();

                IActionBarMessage message = cclib.createActionBarMessage(ChatColor.RED + "Your squid is at " + Math.floor(squid.getHealth() - event.getDamage()) + " health.");
                for (SquidWarsTeam team : teams) {
                    if (team.squid == squid) {
                        for (Player player : team.getPlayers()) {
                            message.sendTo(player);
                        }
                    }
                }

                if (event.getCause() == EntityDamageEvent.DamageCause.DROWNING) {
                    event.setDamage(0.3);
                }
            }

            if (event.getEntity() instanceof Player && event.getDamage() >= ((Player) event.getEntity()).getHealth()) {
                plugin.dieOrRespawn((Player) event.getEntity());
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void hunger(FoodLevelChangeEvent event) {
        event.setFoodLevel(20);
    }

    @EventHandler
    public void playerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (getServer().getOnlinePlayers().size() > 8 || started) {
            for (SquidWarsTeam team : teams) {
                if (team.getPlayers().contains(event.getPlayer()) && team.canRespawn()) {
                    team.rejoinPlayer(player);
                    plugin.dieOrRespawn(player);
                    playersAlive.remove(player.getUniqueId());
                    playersAlive.put(player.getUniqueId(), true);
                    event.setJoinMessage(ChatColor.GOLD + player.getName() + " rejoined!");
                    return;
                }
            }

            event.setJoinMessage(ChatColor.GOLD + player.getName() + " is spectating.");
            player.setGameMode(GameMode.SPECTATOR);
            playersAlive.remove(player.getUniqueId());
            playersAlive.put(player.getUniqueId(), false);
            player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 999999, 1));
            return;
        }
        player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 999999, 0));
        cclib.createActionBarMessage(ChatColor.GOLD + "Left click with an item in hand to select a kit.").sendTo(event.getPlayer());
        player.getInventory().addItem(cclib.createNamedItem(Material.DIAMOND_SWORD, 1, "Class: Rusher"));
        player.getInventory().addItem(cclib.createNamedItem(Material.GOLDEN_APPLE, 1, "Class: Tank"));
        player.getInventory().addItem(cclib.createNamedItem(Material.BOW, 1, "Class: Archer"));
    }

    @EventHandler
    public void inventoryClick(InventoryClickEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void arrowHit(ProjectileHitEvent event) {
        if (event.getEntity().getNearbyEntities(0.5,0.5,0.5).size() >= 1 && event.getEntity().getShooter() instanceof Player) {
            Player player = (Player)event.getEntity().getShooter();
            player.playSound(player.getLocation(), Sound.ORB_PICKUP, 1, 0.75f);
            event.getEntity().remove();
        }
    }

    @EventHandler
    public void mobSpawn(CreatureSpawnEvent event) {
        if (
                event.getEntity().getType() != EntityType.ARROW &&
                event.getEntity().getType() != EntityType.PLAYER &&
                event.getEntity().getType() != EntityType.SQUID
        ) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void blockBreak(BlockBreakEvent event) {
        if (!started) {
            event.setCancelled(true);
        } else {
            event.setCancelled(true);
            event.getBlock().setType(Material.AIR);
            Location location = event.getBlock().getLocation();
            location.getWorld().dropItem(location, new ItemStack(Material.WOOL, 1));
        }
    }

    @EventHandler
    public void blockPlace(BlockPlaceEvent event) {
        if (!started) {
            event.setCancelled(true);
        } else {
            for (SquidWarsTeam team : teams) {
                if (team.getPlayers().contains(event.getPlayer())) {
                    event.getBlockPlaced().setType(Material.WOOL);
                    BlockState state = event.getBlockPlaced().getState();
                    Wool wool = (Wool) state.getData();
                    wool.setColor(new ColorConverter(team.teamColor).getWoolColor());
                    state.update();
                }
            }
        }
    }

    @EventHandler
    public void playerInteract(PlayerInteractEvent event) {
        if (!started) {
            if (event.getItem() != null && event.getItem().getItemMeta().getDisplayName() != null) {
                switch (event.getPlayer().getInventory().getItemInHand().getItemMeta().getDisplayName()) {
                    case "Class: Rusher":
                        if (kits.get(event.getPlayer().getUniqueId()) != SquidWarsKit.RUSHER) {
                            kits.remove(event.getPlayer().getUniqueId());
                            kits.put(event.getPlayer().getUniqueId(), SquidWarsKit.RUSHER);
                            cclib.createActionBarMessage(ChatColor.GOLD + "Selected the RUSHER kit.").sendTo(event.getPlayer());
                            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.NOTE_PLING, 1, 1);
                        }
                        break;
                    case "Class: Tank":
                        if (kits.get(event.getPlayer().getUniqueId()) != SquidWarsKit.TANK) {
                            kits.remove(event.getPlayer().getUniqueId());
                            kits.put(event.getPlayer().getUniqueId(), SquidWarsKit.TANK);
                            cclib.createActionBarMessage(ChatColor.GOLD + "Selected the TANK kit.").sendTo(event.getPlayer());
                            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.NOTE_PLING, 1, 1);
                        }
                        break;
                    case "Class: Archer":
                        if (kits.get(event.getPlayer().getUniqueId()) != SquidWarsKit.ARCHER) {
                            kits.remove(event.getPlayer().getUniqueId());
                            kits.put(event.getPlayer().getUniqueId(), SquidWarsKit.ARCHER);
                            cclib.createActionBarMessage(ChatColor.GOLD + "Selected the ARCHER kit.").sendTo(event.getPlayer());
                            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.NOTE_PLING, 1, 1);
                        }
                        break;
                }
            }
            cclib.getGameUtils().blockInteract(event);
        }
    }

    @EventHandler
    public void breakPainting(HangingBreakEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void damage(EntityDamageByEntityEvent event) {
        if (event.getEntity().getType() == EntityType.SQUID) {
            Squid squid = (Squid) event.getEntity();

            for (SquidWarsTeam team : teams) {
                if (team.squid == squid) {
                    for (Player player : team.getPlayers()) {
                        if (event.getDamager() instanceof Player && event.getDamager().getUniqueId() == player.getUniqueId()) {
                            event.setCancelled(true);
                            return;
                        }
                    }
                    for (Player player : team.getPlayers()) {
                        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 0.5f);
                    }
                }
            }

            if (event.getFinalDamage() >= squid.getHealth()) {
                for (SquidWarsTeam team : teams) {
                    if (team.squid == squid) {
                        for (Player player : team.getPlayers()) {
                            player.playSound(player.getLocation(), Sound.ZOMBIE_PIG_DEATH, 2, 0.3f);
                            player.sendTitle(ChatColor.RED + "YOUR SQUID IS DEAD", "You can't respawn!");
                            getScheduler().runTaskLater(plugin, player::resetTitle, 20 * 2);
                        }
                    }
                }
            }

            if (event.getDamage() >= squid.getHealth() && event.getDamager() instanceof Player) {
                Player killer = (Player) event.getDamager();
                cclib.getGameUtils().awardGems(killer, GemAmount.LARGE);
            }

            event.setCancelled(true);

            if (event.getFinalDamage() >= squid.getHealth()) {
                ((Squid) event.getEntity()).setHealth(((Squid) event.getEntity()).getHealth() - event.getFinalDamage());
            } else {
                event.getEntity().remove();
            }
        } else if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            Player player = (Player) event.getEntity();
            Player damager = (Player) event.getDamager();

            if (player.hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE)) {
                event.setCancelled(true);
                return;
            }

            SquidWarsTeam team1 = null;
            SquidWarsTeam team2 = null;

            for (SquidWarsTeam team : teams) {
                if (team.getPlayers().contains(player)) {
                    team1 = team;
                }
                if (team.getPlayers().contains(damager)) {
                    team2 = team;
                }
            }

            if (team1 != null && team2 != null) {
                if (team1 == team2) {
                    event.setCancelled(true);
                    return;
                }

                if (event.getDamage() >= player.getHealth()) {
                    if (team2.isAlive()) {
                        cclib.getGameUtils().awardGems(damager, GemAmount.MEDIUM);
                    } else {
                        cclib.getGameUtils().awardGems(damager, GemAmount.LARGE);
                    }
                }
            }
        }
    }

    @EventHandler
    public void kill(PlayerDeathEvent event) {
        Player killer  = event.getEntity().getKiller();
        if (killer != null) {
            cclib.getGameUtils().awardGems(killer, GemAmount.MEDIUM);
        }
    }

    @EventHandler
    public void disconnect(PlayerQuitEvent event) {
        playersAlive.remove(event.getPlayer().getUniqueId());
        playersAlive.put(event.getPlayer().getUniqueId(), false);
        }

    @EventHandler
    public void disconnect(PlayerKickEvent event) {
        playersAlive.remove(event.getPlayer().getUniqueId());
        playersAlive.put(event.getPlayer().getUniqueId(), false);
    }

    @EventHandler
    public void eat(PlayerItemConsumeEvent event) {
        if (!started) {
            event.setCancelled(true);
        }
    }
}
