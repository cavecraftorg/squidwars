package top.cavecraft.squidwars;

import org.bukkit.*;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;
import top.cavecraft.cclib.ICCLib;
import top.cavecraft.cclib.ICCLib.IGameUtils.GemAmount;
import top.cavecraft.cclib.ICCLib.ITickReq;

import java.util.*;

public final class SquidWars extends JavaPlugin {
    static boolean started = false;
    static List<ITickReq> tickRequirements;

    static Location[] islands;
    static final List<ChatColor> teamColors = Arrays.asList(
            ChatColor.RED,
            ChatColor.BLUE,
            ChatColor.GREEN,
            ChatColor.YELLOW
    );
    static List<SquidWarsTeam> teams = new ArrayList<>();
    static SquidWars plugin;
    static ICCLib cclib;

    static ICCLib.IScoreboardManager scoreboardManager;
    boolean awardedWinner = false;
    int teamsLeft;

    public static Map<UUID, SquidWarsKit> kits = new HashMap<>();
    public static Map<UUID, Boolean> playersAlive = new HashMap<>();

    @Override
    public void onEnable() {
        islands = new Location[] {
                new Location(Bukkit.getWorlds().get(0), 0, 64.5, -40, 0, 0),
                new Location(Bukkit.getWorlds().get(0), 0, 64.5, 40, -180, 0),
                new Location(Bukkit.getWorlds().get(0), -40, 64.5, 0, -90, 0),
                new Location(Bukkit.getWorlds().get(0), 40, 64.5, 0, 90, 0)
        };

        plugin = this;

        cclib = (ICCLib) Bukkit.getPluginManager().getPlugin("CCLib");

        tickRequirements = Arrays.asList(
                cclib.createTickReq(2, 50, true),
                cclib.createTickReq(3, 40, true),
                cclib.createTickReq(4, 30, true),
                cclib.createTickReq(5, 25, true),
                cclib.createTickReq(6, 20, true),
                cclib.createTickReq(7, 15,true),
                cclib.createTickReq(8, 10, true)
        );

        cclib.enableGameFeatures(tickRequirements, this::startGame, this::teleportToArena);

        scoreboardManager = cclib.getScoreboardManager();

        Collections.shuffle(teamColors);

        scoreboardManager.setTitleOfAll(ChatColor.GOLD + "SQUID WARS");
        scoreboardManager.addBlankLineToAll();
        scoreboardManager.addLineToAll("synopsistitle", ChatColor.GOLD + "Synopsis:");
        scoreboardManager.addLineToAll("synopsis1", ChatColor.WHITE + "Squid Wars takes place in the");
        scoreboardManager.addLineToAll("synopsis2", ChatColor.WHITE + "sky. Mine blocks and bridge");
        scoreboardManager.addLineToAll("synopsis3", ChatColor.WHITE + "to the other islands to kill");
        scoreboardManager.addLineToAll("synopsis4", ChatColor.WHITE + "their squid. Kill all other");
        scoreboardManager.addLineToAll("synopsis5", ChatColor.WHITE + "players and squids to win!");
        scoreboardManager.addBlankLineToAll();
        scoreboardManager.addLineToAll("classtitle", ChatColor.GOLD + "Picking a class:");
        scoreboardManager.addLineToAll("class1", ChatColor.WHITE + "Tank can take many hits.");
        scoreboardManager.addLineToAll("class2", ChatColor.WHITE + "Archer can kill at range.");
        scoreboardManager.addLineToAll("class3", ChatColor.WHITE + "Rusher gets to mid fastest.");
        scoreboardManager.addBlankLineToAll();
        scoreboardManager.addLineToAll("watermark", ChatColor.YELLOW + "-       cavecraft.top       -");

        getServer().getPluginManager().registerEvents(new SquidWarsEvents(), this);
        getServer().getScheduler().runTaskTimer(this, this::gameTick, 0, 1);
    }

    public void teleportToArena() {
        Collection<? extends Player> players = getServer().getOnlinePlayers();

        for (Player player : players) {
            player.getInventory().clear();
        }

        int teamCount = 2;
        switch (players.size()) {
            case 2:
            case 4:
                teamCount = 2;
                break;
            case 3:
            case 5:
            case 6:
            case 7:
                teamCount = 3;
                break;
            case 8:
                teamCount = 4;
                break;
        }

        List<? extends Player> playersGoingToTeams = new ArrayList<>(players);
        Collections.shuffle(playersGoingToTeams);
        for (int i = 0; i < teamCount; i++) {
            SquidWarsTeam team = new SquidWarsTeam(teamColors.get(i), islands[i].clone());
            for (int j = 0; j < (int)Math.floor(players.size() / ((double)teamCount)); j++)  {
                team.addPlayer(playersGoingToTeams.get(0));
                playersGoingToTeams.remove(0);
            }
            teams.add(team);
        }
        if (playersGoingToTeams.size() > 0) {
            int i = 0;
            for (Player player : playersGoingToTeams) {
                teams.get(i).addPlayer(player);
                i++;
            }
        }
    }

    public void startGame() {
        started = true;

        Collection<? extends Player> players = getServer().getOnlinePlayers();
        teamsLeft = teams.size();

        for (Player player : players) {
            kits.getOrDefault(player.getUniqueId(), SquidWarsKit.TANK).setKit(player);
        }

        scoreboardManager.clearAll();
        scoreboardManager.addBlankLineToAll();
        for (SquidWarsTeam team : teams) {
            scoreboardManager.addLineToAll(team.teamColor.name(), team.teamColor + team.teamColor.name() + " TEAM ");
            scoreboardManager.setDynamicOfAll(team.teamColor.name(), team.teamStatusString());
        }
        scoreboardManager.addBlankLineToAll();
        scoreboardManager.addLineToAll("watermark", ChatColor.YELLOW + "-    cavecraft.top    -");

        getServer().getScheduler().runTaskTimer(this, () -> {
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (kits.get(player.getUniqueId()).equals(SquidWarsKit.ARCHER)) {
                    player.getInventory().addItem(new ItemStack(Material.ARROW));
                }
            }
        }, 300, 300);

        getServer().getScheduler().runTaskTimer(this, this::awardCoins, 1200, 1200);
    }

    public void awardCoins() {
        Collection<? extends Player> players = getServer().getOnlinePlayers();
        for (Player player : players) {
            if (playersAlive.get(player.getUniqueId())) {
                cclib.getGameUtils().awardGems(player, GemAmount.SMALL);
            }
        }
    }

    public void gameTick() {
        if (started) {
            playerTick();
        }

        teamsLeft = 0;
        for (SquidWarsTeam team : teams) {
            if (team.isAlive()) {
                teamsLeft++;
            }
        }

        if (started && teamsLeft <= 1 && !awardedWinner) {
            awardWinner();
        }
    }

    public void awardWinner() {
        awardedWinner = true;
        Collection<? extends Player> players = getServer().getOnlinePlayers();
        for (Player player : players) {
            if (playersAlive.get(player.getUniqueId())) {
                cclib.getGameUtils().awardGems(player, GemAmount.EXTRA_LARGE);
                cclib.getGameUtils().winnerEffect(player);
            }
        }
        for (SquidWarsTeam team : teams) {
            if (team.isAlive()) {
                getServer().broadcastMessage(team.teamColor + team.teamColor.name() + " team won the game!");
            }
        }
        getServer().getScheduler().runTaskLater(this, cclib::restartServer, 160);
    }

    public void playerTick() {
        for (SquidWarsTeam team : teams) {
            scoreboardManager.setDynamicOfAll(team.teamColor.name(), team.teamStatusString());
        }
        if (!awardedWinner) {
            Collection<? extends Player> players = getServer().getOnlinePlayers();
            for (Player player : players) {
                if (player.getLocation().getY() <= 0 || player.getLocation().distance(new Location(Bukkit.getWorlds().get(0), 0, 65, 0)) >= 60) {
                    dieOrRespawn(player);
                }
            }
        }
    }

    public void dieOrRespawn(Player player) {
        boolean canRespawn = false;
        for (SquidWarsTeam team : teams) {
            if (team.getPlayers().contains(player) && team.canRespawn()) {
                canRespawn = true;
                cclib.getGameUtils().deathMessage(player);
                getServer().getScheduler().runTaskLater(this, () -> {
                    for (PotionEffect potionEffect : player.getActivePotionEffects()) {
                        player.removePotionEffect(potionEffect.getType());
                    }
                    player.setGameMode(GameMode.SURVIVAL);
                    player.getInventory().clear();
                    kits.getOrDefault(player.getUniqueId(), SquidWarsKit.TANK).setRespawnKit(player);
                    player.setHealth(20);
                    player.setVelocity(new Vector(0, 0, 0));
                    player.setFallDistance(0);
                    player.setNoDamageTicks(0);
                    player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 60, 1, true));
                    player.teleport(new Location(team.spawn.getWorld(), team.spawn.getX(), Bukkit.getWorlds().get(0).getHighestBlockAt(team.spawn).getY() + 1.0, team.spawn.getZ()));
                }, 20 * 5);
            }
        }
        if (!canRespawn) {
            playersAlive.remove(player.getUniqueId());
            playersAlive.put(player.getUniqueId(), false);
            cclib.getGameUtils().deathEffect(player);
        }
    }
}
