package top.cavecraft.squidwars;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Squid;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static top.cavecraft.squidwars.SquidWars.playersAlive;
import static top.cavecraft.squidwars.SquidWars.scoreboardManager;

public class SquidWarsTeam {
    private List<UUID> playerUUIDS = new ArrayList<>();
    Squid squid;
    ChatColor teamColor;
    Location spawn;

    SquidWarsTeam(ChatColor teamColor, Location spawn) {
        this.squid = (Squid) Bukkit.getWorlds().get(0).spawnEntity(spawn.clone(), EntityType.SQUID);
        this.squid.setMaxHealth(100);
        this.squid.setHealth(100);
        this.teamColor = teamColor;
        this.spawn = spawn;
    }

    public void addPlayer(Player player) {
        playerUUIDS.add(player.getUniqueId());
        rejoinPlayer(player);
        player.teleport(spawn.clone());
        playersAlive.remove(player.getUniqueId());
        playersAlive.put(player.getUniqueId(), true);
    }

    public void rejoinPlayer(Player player) {
        player.setDisplayName(teamColor + teamColor.name() + " TEAM > " + player.getDisplayName());
        player.setPlayerListName(player.getDisplayName());
    }

    public boolean canRespawn() {
        return squid != null && squid.isValid() && !squid.isDead();
    }

    public boolean isAlive() {
        boolean isAlive = canRespawn();
        for (Player player : getPlayers()) {
            if (player != null && playersAlive.getOrDefault(player.getUniqueId(), false) && player.isOnline() && player.isValid()) {
                isAlive = true;
            }
        }
        return isAlive;
    }

    public String teamStatusString() {
        int alivePlayers = 0;
        for (Player player : getPlayers()) {
            if (player != null && playersAlive.getOrDefault(player.getUniqueId(), false) && player.isOnline() && player.isValid()) {
                alivePlayers++;
            }
        }

        if (canRespawn()) {
            return ChatColor.GREEN + "✔";
        } else {
            if (alivePlayers == 0) {
                return ChatColor.RED + "✗";
            } else {
                return ChatColor.YELLOW + String.valueOf(alivePlayers) + "/" + playerUUIDS.size();
            }
        }
    }

    public List<Player> getPlayers() {
        List<Player> players = new ArrayList<>();
        for (UUID uuid : playerUUIDS) {
            players.add(Bukkit.getPlayer(uuid));
        }
        return players;
    }
}
